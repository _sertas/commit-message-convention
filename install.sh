#!/usr/bin/env bash

# Colors definition.
RED='\e[1;31m'
GRN='\e[1;32m'
YWL='\e[1;33m'
BLUE='\e[1;36m'
MAG='\e[1;35m'
END='\e[0m'

# ASCII Banner.
printf "${BLUE} _____                                        _ _\n"
printf "| __  |___ ___ _ _ ___    ___ ___ _____ _____|_| |_    _____ ___ ___ ___ ___ ___ ___ ___\n"
printf "| __ -| -_| -_| | | . |  |  _| . |     |     | |  _|  |     | -_|_ -|_ -| .'| . | -_|_ -|\n"
printf "|_____|___|___|\_/|___|  |___|___|_|_|_|_|_|_|_|_|    |_|_|_|___|___|___|__,|_  |___|___|\n"
printf "                                                                            |___|${END}\n"

# Define variables.
GIT_CONFIGS_ROOT="$HOME/.config/git"
GIT_HOOKS_LOCAL_PATH="./githooks"
GIT_MESSAGE_FILENAME="gitmessage.txt"

# Print introduction.
printf "You're awesome for installing ${BLUE}Beevo's${END} ${YWL}𝗚it${END} commit convention!"
printf "This way, we'll be able to standardize and write better commit messages!\n"
printf "\nStarting...\n\n"

printf "${BLUE}Where do you want to install the ${YWL}𝗚it${BLUE} configs? [${YWL}$GIT_CONFIGS_ROOT${BLUE}]: ${END}"

read path
GIT_CONFIGS_ROOT=${path:-$GIT_CONFIGS_ROOT}
GIT_HOOKS_PATH="$GIT_CONFIGS_ROOT/hooks"
GIT_MESSAGE_PATH="$GIT_CONFIGS_ROOT/$GIT_MESSAGE_FILENAME"

printf "\nInstalling ${MAG}githooks${END} and ${MAG}gitmessage${END} into ${YWL}${GIT_CONFIGS_ROOT}${END} ...\n"

# Ensure if install destination exists.
printf "\n${YWL}Step 1${END} - Ensure ${YWL}${GIT_HOOKS_PATH}${END} exists... "
mkdir -p ${GIT_HOOKS_PATH} 2> error.log || { printf "${RED}Χ Failed...\n%s${END}\n" "$(<error.log)"; exit 1; }
printf "${GRN}Success ✓${END}\n"

# Copy scripts to destination
printf "${YWL}Step 2${END} - Copy hooks to ${YWL}${GIT_HOOKS_PATH}${END}... "
cp ${GIT_HOOKS_LOCAL_PATH}/* ${GIT_HOOKS_PATH} 2> error.log || { printf "${RED}Χ Failed...\n%s${END}\n" "$(<error.log)"; exit 1; }
printf "${GRN}Success ✓${END}\n"

# Update git configs.
printf "${YWL}Step 3${END} - Update git global configs and set core.hooksPath to ${YWL}${GIT_HOOKS_PATH}${END}... "
git config --global core.hooksPath ${GIT_HOOKS_PATH} 2> error.log || { printf "${RED}Χ Failed...\n%s${END}\n" "$(<error.log)"; exit 1; }
printf "${GRN}Success ✓${END}\n"

# Ensure scripts are executable.
printf "${YWL}Step 4${END} - Set hooks executable... "
find ${GIT_HOOKS_PATH} -type f -exec chmod +x {} \; 2> error.log || { printf "${RED}Χ Failed...\n%s${END}\n" "$(<error.log)"; exit 1; }
printf "${GRN}Success ✓${END}\n"

# Copy template to destination.
printf "${YWL}Step 5${END} - Copy ${MAG}gitmessage${END} template into ${YWL}${GIT_MESSAGE_PATH}${END} ... "
cp ${GIT_MESSAGE_FILENAME} ${GIT_MESSAGE_PATH} 2> error.log || { printf "${RED}Χ Failed...\n%s${END}\n" "$(<error.log)"; exit 1; }
printf "${GRN}Success ✓${END}\n"

# Update git configs.
printf "${YWL}Step 6${END} - Update git global configs and set commit.template to ${YWL}${GIT_MESSAGE_PATH}${END} ... "
git config --global commit.template ${GIT_MESSAGE_PATH} 2> error.log || { printf "${RED}Χ Failed...\n%s${END}\n" "$(<error.log)"; exit 1; }
printf "${GRN}Success ✓${END}\n"

printf "\n"
# Done
printf "${GRN}Done ✓${END} Easy peasy!\n"
