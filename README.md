# Commit Message Convention

## Getting started

To install beevo's commit message conventions, you just need to run the install bash script.

```bash
./install.sh
```

In short, this will update your **global git variables** and set the new paths where **hooks** and **commit template** are stored.