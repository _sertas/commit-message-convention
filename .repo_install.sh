#!/usr/bin/env bash

# Colors definition.
RED='\e[1;31m'
GRN='\e[1;32m'
YWL='\e[1;33m'
BLUE='\e[1;36m'
MAG='\e[1;35m'
END='\e[0m'

# ASCII Banner.
printf "${BLUE} _____                                        _ _\n"
printf "| __  |___ ___ _ _ ___    ___ ___ _____ _____|_| |_    _____ ___ ___ ___ ___ ___ ___ ___\n"
printf "| __ -| -_| -_| | | . |  |  _| . |     |     | |  _|  |     | -_|_ -|_ -| .'| . | -_|_ -|\n"
printf "|_____|___|___|\_/|___|  |___|___|_|_|_|_|_|_|_|_|    |_|_|_|___|___|___|__,|_  |___|___|\n"
printf "                                                                            |___|${END}\n"

# Define variables.
GIT_HOOKS_PATH="$(pwd)/githooks"
GIT_MESSAGE_PATH="$(pwd)/gitmessage.txt"

# Print introduction.
printf "You're awesome for installing ${BLUE}Beevo's${END} ${YWL}𝗚it${END} commit convention!"
printf "This way, we'll be able to standardize and write better commit messages!\n"
printf "\nStarting...\n\n"

printf "Installing ${MAG}githooks${END} and ${MAG}gitmessage${END} ...\n\n"

# Update git configs.
printf "${YWL}Step 1${END} - Update git global configs and set core.hooksPath to ${YWL}${GIT_HOOKS_PATH}${END}... "
git config --global core.hooksPath ${GIT_HOOKS_PATH} 2> error.log || { printf "${RED}Χ Failed...\n%s${END}\n" "$(<error.log)"; exit 1; }
printf "${GRN}Success ✓${END}\n"

# Ensure scripts are executable.
printf "${YWL}Step 2${END} - Set hooks executable... "
find ${GIT_HOOKS_PATH} -type f -exec chmod +x {} \; 2> error.log || { printf "${RED}Χ Failed...\n%s${END}\n" "$(<error.log)"; exit 1; }
printf "${GRN}Success ✓${END}\n"

# Update git configs.
printf "${YWL}Step 3${END} - Update git global configs and set commit.template to ${YWL}${GIT_MESSAGE_PATH}${END} ... "
git config --global commit.template ${GIT_MESSAGE_PATH} 2> error.log || { printf "${RED}Χ Failed...\n%s${END}\n" "$(<error.log)"; exit 1; }
printf "${GRN}Success ✓${END}\n"

printf "\n"
# Done
printf "${GRN}Done ✓${END} Easy peasy!\n"

